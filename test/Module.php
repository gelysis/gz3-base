<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * This module class is for internal use only - do not re-use!
 * @package Gz3BaseTest\Fixture
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

declare(strict_types = 1);
namespace Gz3BaseTest;

use Gz3Base\Module as Gz3BaseModule;


class Module extends Gz3BaseModule
{

    /** @var \ReflectionClass $this->reflection */
    /** @var string $this->namespace */
    /** @var string $this->directory */

    /**
     * @return string $namespaceAutoloaderPath
     */
    protected function getNamespaceAutoloaderPath() : string
    {
        return $this->getModuleRootDirectory().'/test';
    }

    /**
     * @return string $classFileDirectory
     */
    protected function getModuleRootDirectory() : string
    {
        parent::getModuleRootDirectory();

        return dirname($this->directory);
    }

}
