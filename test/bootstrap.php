<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * Generic phpunit bootstrap file
 * @package Gz3BaseTest
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

declare(strict_types = 1);
error_reporting(E_ALL | E_STRICT);

$root = realpath(strstr(__FILE__, 'test'.DIRECTORY_SEPARATOR, true));
chdir($root);

include 'init_autoloader.php';
require_once 'src/Test/PhpUnit/TestInitialiser.php';
$applicationConfig = include 'config/application.config.php';

\Gz3Base\Test\PhpUnit\TestInitialiser::init($applicationConfig);
