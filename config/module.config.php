<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * @package Gz3Base\config
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

$moduleConfig = [
    'di'=>[
        'definition'=>[],
        'instance'=>[]
    ],
    'controllers'=>[
        'initializers'=>[],
        'invokables'=>[]
    ],
    'controller_plugins'=>[
        'factories'=>[],
        'invokables'=>[]
    ]
];

return $moduleConfig;
