<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * Application config, purely for phpunit testing
 * @package Gz3Base\config
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

$applicationConfig = [
    'modules'=>[
        'Gz3Base'
    ],
    'module_listener_options'=>[
        'module_paths'=>[
            './src',
            'test'=>'./test'
        ],
        'config_glob_paths'=>[
            'config/autoload/{{,*.}global,{,*.}local}.php'
        ],
        'config_cache_enabled'=>false,
        'config_cache_key'=>false,
        'module_map_cache_enabled'=>false,
        'module_map_cache_key'=>false,
        'cache_dir'=>'data/cache/',
    ]
];

return $applicationConfig;

