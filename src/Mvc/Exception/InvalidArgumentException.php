<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * @package Gz3Base\Exception
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

declare(strict_types = 1);
namespace Gz3Base\Mvc\Exception;

use Gz3Base\Mvc\Exception\ExceptionTrait;
use Gz3Base\Record\RecordableInterface;
use Gz3Base\Record\RecordableTrait;
use Zend\Mvc\Exception\InvalidArgumentException as ZendInvalidArgumentException;


class InvalidArgumentException extends ZendInvalidArgumentException
    implements RecordableInterface
{
    use ExceptionTrait, RecordableTrait;

    /** @var bool self::INIT_RECORDING */
    const INIT_RECORDING = false;
    /** @var bool self::DEINIT_RECORDING */
    const DEINIT_RECORDING = false;

}
