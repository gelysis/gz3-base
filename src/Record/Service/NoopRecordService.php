<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * @package Gz3Base\Service
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

declare(strict_types = 1);
namespace Gz3Base\Record\Service;

use Gz3Base\Record\Service\RecordService;


class NoopRecordService extends RecordService
{

    /**
     * {@inheritDoc}
     * @see \Zend\Log\Logger::record()
     */
    public function log($priority, $message, $extra = [])
    {
        try {
            $recordService = parent::log($priority, $message, $extra = []);
        }catch (\Zend\Log\Exception\ExceptionInterface $exception) {}

        return $this;
    }

}
