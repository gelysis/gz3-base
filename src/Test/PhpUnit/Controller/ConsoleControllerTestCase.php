<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * @package Gz3Base\Controller
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

declare(strict_types = 1);
namespace Gz3Base\Test\Controller;

use Zend\Test\PHPUnit\Controller\AbstractConsoleControllerTestCase;


class ConsoleControllerTestCase extends AbstractConsoleControllerTestCase
{
    use ControllerTestCaseTrait;
}
