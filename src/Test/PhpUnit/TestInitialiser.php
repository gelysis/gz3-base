<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * @package Gz3Base
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

declare(strict_types = 1);
namespace Gz3Base\Test\PhpUnit;

use Zend\Loader\StandardAutoloader;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
use Zend\Test\Util\ModuleLoader;


class TestInitialiser
{

    /** @var ServiceManager self::$serviceManager */
    protected static $serviceManager;


    /**
     * @param array $configuration
     * @param string[] $modulePaths
     */
    public static function init(array $configuration)
    {
        $modules = [];
        $loader = new StandardAutoloader();

        $modulePaths = [];
        foreach ($configuration['module_listener_options']['module_paths'] as $modulePath) {
            foreach ($configuration['modules'] as $key=>$module) {
                if (strpos($module, '\\') !== false) {
                    $rootSpace = strstr($module, '\\', true);
                    $relativePath = '/'.str_replace([$rootSpace.'\\', '\\'], ['', '/'], $module);
                }else {
                    $rootSpace = $module;
                    $relativePath = '';
                }
                $isTestSpace = preg_match('#tests?#ism', (string) $key) || preg_match('#/tests?$#sm', $modulePath);

                if ($isTestSpace) {
                    $module = str_replace($rootSpace, $rootSpace.'Test', $module);
                }

                $fullPath = realpath($modulePath.$relativePath);
                if ($fullPath) {
                    $modulePaths[$module] = $fullPath;
                }
            }
        }

        foreach ($modulePaths as $module=>$path) {
            $modules[] = $module;
            $loader->registerNamespace($module, $path);
        }
        $loader->register();

        $configuration['modules'] = $modules;
        self::setServiceManager($configuration);

        self::getServiceManager()->get('ModuleManager')->loadModules();
    }

    /**
     * @param array $config
     * @return void
     */
    protected static function setServiceManager(array $config)
    {
        // Prepare the service manager
        $serviceManagerConfig = isset($config['service_manager']) ? $config['service_manager'] : [];
        $serviceManagerConfig = new ServiceManagerConfig($serviceManagerConfig);

        self::$serviceManager = new ServiceManager($config);
        $serviceManagerConfig->configureServiceManager(self::$serviceManager);
        self::$serviceManager->setService('ApplicationConfig', $config);
//        self::$serviceManager->setService('ConfigService', $config);
    }

    /**
     * @return ServiceManager static::$serviceManager
     */
    public static function getServiceManager() : ServiceManager
    {
        return self::$serviceManager;
    }

}
