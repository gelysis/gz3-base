<?php
/**
 * Gz3Base - Zend Framework Base Tweaks / Zend Framework Basis Anpassungen
 * This module class is for internal use only - do not re-use!
 * @package Gz3Base
 * @author gelysis <andreas@gelysis.net>
 * @copyright ©2016-2017 Andreas Gerhards - All rights reserved
 * @license BSD-3-Clause (http://opensource.org/licenses/BSD-3-Clause)
 */

declare(strict_types = 1);
namespace Gz3Base;


class Module extends AbstractModule
{

    /** @var \ReflectionClass $this->reflection */
    /** @var string $this->namespace */
    /** @var string $this->directory */


    /**
     * {@inheritDoc}
     * @see \Gz3Base\AbstractModule::init()
     */
    public function init()
    {
    }

    /**
     * @return string $namespacePath
     */
    protected function getNamespacePath() : string
    {
        $namespacePath = str_replace('\\', '/', preg_replace('#^GzBase(Test)?\\\#', '', $this->getNamespace()));

        return $namespacePath;
    }

}
